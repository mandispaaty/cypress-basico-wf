
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));

    it("fills all the text input fields", () => {
        const firstName = "Paaty";
        const lastName = "Silva";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("1@t");
        cy.get("#requests").type("Teste Cypress");
        cy.get("#signature").type(`${firstName} ${lastName}`); // neste caso usar a crase `

    });

    it("select four tickets", () => {
        cy.get("#ticket-quantity").select("4");

    });
    
     it("select 'vip' ticket type", () => {
        cy.get("#vip").check(); //ou click()

    });

      it("selects 'social media' checkbox", () => {
        cy.get("#friend").check(); 
        cy.get("#social-media").check();

    });

       it("selects 'friend' and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check(); 
        cy.get("#social-media").check();
        cy.get("#friend").uncheck(); //or click

    });

    it("has 'TICKETBOX' header's heading", () => { 
        cy.get("h1").should("contain", "TICKETBOX"); // poderia ser ("header h1")
    });

    it("alerts on invalid email", () => { 
        cy.get("#email")
            .as("email") //apelido
            .type("mandispaatygmail.com");

        cy.get("#email.invalid").should("exist"); // ou quando o email for correto cy.get("#email.invalid").should("not.exist");

        cy.get("@email") //como ja tenho um apelido preciso usar o @ para chamar
            .clear()
            .type("mandispaaty@gmail.com");
    });
        
});