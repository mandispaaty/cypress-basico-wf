
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));

    it("Teste e2e", () => {
        const firstName = "Paaty";
        const lastName = "Silva";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("mandispaaty@gmail.com");
        cy.get("#ticket-quantity").select("3");
        cy.get("#general").check();
        cy.get("#friend").check();
        cy.get("#social-media").check();
        cy.get("#requests").type("Testando mais funcionalidades");
        cy.get("legend").should("contain", "Purchase Agreement");

        cy.get(".agreement p")
            .should("contain",
                `I, ${fullName}, wish to buy 3 General Admission tickets. I understand that all ticket sales are final.`)
        cy.get("#agree").check();
        cy.get("#signature").type(`${fullName}`);
        //verificar que o botao nao esta desabilitado
        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled")


        cy.get(" .reset").should("exist").click();
        //verifica que o botao esta desabilitado 
        cy.get("@submitButton").should("be.disabled")

        //criacao de comandos customizados

        it("fills mandatory fields using suport command", () => {
            const customer = {
                firstName: "Pedro",
                lastName: "Gustavo",
                email: "pg@example.com"
            }

            cy.fillMandatoryFields(customer);
            cy.get("button[type='submit']")
                .as("submitButton")
                .should("not.be.disabled")

            cy.get("#agree").uncheck();

            cy.get("@submitButton").should("be.disabled")

        });


    });


});
