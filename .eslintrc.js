module.exports = {
    root: true,
    parserOptions: {
      parser: "babel-eslint",
    },
    env: {
      node: true,
      "cypress/globals": true,
    },
    plugins: ["cypress"],
    extends: ["eslint:recommended"],
    rules: {
      "cypress/no-assigning-return-values": "warn",
      "cypress/no-unnecessary-waiting": "warn",
      "cypress/assertion-before-screenshot": "warn",
      "cypress/no-force": "warn",
      "cypress/no-async-tests": "error"
    },
  };